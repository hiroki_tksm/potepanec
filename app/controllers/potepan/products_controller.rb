class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_AMOUNT = 4

  def show
    @product             = Spree::Product.find(params[:id])
    @product_category_id = @product.taxon_ids.first
    @related_products    = Spree::Product.in_taxons(@product.taxons).includes(master: [:default_price, :images]).where.not(id: @product.id).distinct.limit(RELATED_PRODUCTS_MAX_AMOUNT)
  end
end
