require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create :product, taxons: [taxon] }

    before { get :show, params: { id: taxon.id } }

    it 'showテンプレートが表示されていること' do
      expect(response).to render_template :show
    end

    it '@taxonomyに意図したデータがアサインしていること' do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    it '@taxonに意図したデータがアサインしていること' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@productに意図したデータがアサインしていること' do
      expect(assigns(:products)).to contain_exactly(product)
    end
  end
end
