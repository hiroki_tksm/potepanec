require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:unrelated_product)  { create(:product) }

    before { get :show, params: { id: product.id } }

    it 'showテンプレートが表示されていること' do
      expect(response).to render_template :show
    end

    it '@productに意図したデータがアサインしていること' do
      expect(assigns(:product)).to eq product
    end

    it '@related_productsの数が４件になっていること' do
      expect(assigns(:related_products).count).to eq 4
    end

    it '関連性のない商品が@related_productsに含まれていないこと' do
      expect(assigns(:related_products)).not_to include unrelated_product
    end
  end
end
