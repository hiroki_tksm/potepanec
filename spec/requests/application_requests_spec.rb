require 'rails_helper'

RSpec.describe "ApplicationRequests", type: :helper do
  let!(:base_title) { "BIGBAG Store" }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create :taxon, taxonomy: taxonomy }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "GET #index" do
    it 'タイトル表示に成功していること' do
      visit potepan_path
      expect(page).to have_title "#{base_title}"
    end
  end

  describe "GET #show" do
    it 'productページで動的なタイトル表示に成功していること' do
      visit potepan_product_path(id: product.id)
      expect(page).to have_title "#{product.name} - #{base_title}"
    end
    it 'categoryページで動的なタイトル表示に成功していること' do
      visit potepan_category_path(id: taxon.id)
      expect(page).to have_title "#{taxon.name} - #{base_title}"
    end
  end
end
