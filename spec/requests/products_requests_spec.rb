require 'rails_helper'

RSpec.describe "ProductsRequests", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create(:product, taxons: [taxon]) }

    it 'リクエストが成功すること' do
      get potepan_product_path(product.id)
      expect(response.status).to eq(200)
    end
  end
end
