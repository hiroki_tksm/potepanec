require 'rails_helper'

RSpec.describe "CategoriesRequests", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }

    it 'リクエストが成功すること' do
      get potepan_category_path(id: taxon.id)
      expect(response.status).to eq(200)
    end
  end
end
