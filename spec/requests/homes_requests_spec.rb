require 'rails_helper'

RSpec.describe "HomesRequests", type: :request do
  describe "GET #index" do
    it "リクエストが成功すること" do
      get '/potepan/index'
      expect(response.status).to eq(200)
    end
  end
end
