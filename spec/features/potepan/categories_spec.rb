require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create :taxon, taxonomy: taxonomy }
  let!(:product) { create :product, taxons: [taxon] }
  let!(:other_product) { create(:product) }

  describe "GET #show" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it '画面表示ができていること' do
      expect(page).to have_content(taxon.name)
      expect(page).to have_content(product.name)
      expect(page).to have_content(product.display_price)
      expect(page).to have_no_content(other_product.name)
    end

    it '商品名から商品詳細へのリンクが機能していること' do
      expect(page).to have_link product.name
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
