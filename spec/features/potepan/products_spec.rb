require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxonomy_1) { create(:taxonomy, name: 'Category') }
  let!(:taxonomy_2) { create(:taxonomy, name: 'Brand') }
  let!(:taxon_1) { create(:taxon, taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id) }
  let!(:taxon_2) { create(:taxon, taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id) }
  let!(:taxon_3) { create(:taxon, taxonomy: taxonomy_2, parent_id: taxonomy_2.root.id) }
  let!(:product) { create(:product, taxons: [taxon_1]) }
  let!(:product_category) { product.taxon_ids.first }
  let!(:other_product) { create(:product) }
  let!(:related_product) { create(:product, taxons: [taxon_1]) }
  let!(:unrelated_taxon_product) { create(:product, taxons: [taxon_2]) }
  let!(:unrelated_taxonomy_product) { create(:product, taxons: [taxon_3]) }

  describe "GET #show" do
    before do
      visit potepan_product_path(product.id)
    end

    it 'メイン商品のみ表示ができていること' do
      within '.singleProduct' do
        expect(page).to have_content(product.name)
        expect(page).to have_content(product.display_price)
        expect(page).to have_no_content(other_product.name)
      end
    end

    it '関連商品のみ表示ができていること' do
      within '.productsContent' do
        expect(page).to have_content(related_product.name)
        expect(page).to have_content(related_product.display_price)
        expect(page).to have_no_content(unrelated_taxon_product.name)
        expect(page).to have_no_content(unrelated_taxonomy_product.name)
      end
    end

    it '関連商品名から商品詳細へのリンクが機能していること' do
      expect(page).to have_link related_product.name
      click_on related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it '「一覧ページに戻る」のリンクが@productのカテゴリーに紐づくカテゴリーページへ遷移すること' do
      expect(page).to have_link '一覧ページへ戻る'
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(product_category)
    end
  end
end
